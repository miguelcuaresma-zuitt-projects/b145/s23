// CRUD Operations

//Create
	// - to insert documents

// insertOne() method
db.collections.users.insertOne({document});

db.users.insertOne(
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: 123456,
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	}
);

//insertMany() method

// db.collections.insertMany([{doc1}, {{doc2}}])

db.users.insertMany(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: 789456,
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 50,
			contact: {
				phone: 789456,
				email: "neilarmstrong@gmail.com"
			},
			courses: ["Python", "Laravel", "PHP"],
			department: "none"
		},

	]
);

//Read Operation
	// - retrieves documents from the collection

// db.collections.find({query}, {field projection})
//find() method
db.users.find();

//Update Operation
	// - updates a document
// db.collections.updateOne({filter}, {update})
// db.collection.updateMany()

//updateOne() method
// add document to be modified first
db.users.insertOne(
    {
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "contact": {
                "phone": "0",
                "email": "test@gmail.com"
        },
        "courses": [],
        "department": "none"
    }
);

db.users.updateOne(
	{_id:ObjectId("61e7fdb330362727d17bd4b1")}
	{ $set: {firstName: "Bill"}},
	{lastName: "Gates"},
	{phone: 12345678},
	{email: "bill@gmail.com"},
	{courses: ["PHP", "Laravel", "HTML"]},
	{department: "operations"},
	{status: "active"}
);

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "hr"}
	}
);

db.users.updateOne(
	{_id: ObjectId("61e7fdb330362727d17bd4b1")},
	{
	$unset: { status: "active"}	
	}
);

//Delete Operation
	//removes document from a collection

	db.collection.deleteOne()
	db.collections.deleteMany()

	// insert a document as an example to be deleted
	db.users.insertOne({firstName: "Joy", lastName: "Pague"})

	db.users.insertOne({firstName: "Bill", lastName: "Crawford"});

	db.users.deleteMany(
	{ firstName: "Bill"}
	)